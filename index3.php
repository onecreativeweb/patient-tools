<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<link href="icheck-1.x/skins/square/green.css" rel="stylesheet">
<script src="icheck-1.x/icheck.js"></script>
<script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
<script src="jquery.circliful.min.js"></script>
<body>
<div class="container-fluid" id="header">
  <div class="row">
    <div class="col-sm-3 text-center">
      <img id="pic-logo" src="image/logo.jpg">
    </div>
    <div class="col-sm-5 text-center col-sm-offset-1">
        <div class="row">
          <div class="col-sm-9 title" >
            <p>Welcome Wayne Enterpises Members</p>
          </div>
          <div class="col-sm-3">
            <img id="pic-cnlogo" src="image/cnlogo.jpg">
          </div>
        </div>
    </div>
    <div class="col-sm-2 text-center col-sm-offset-1">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <div id="menu-icon-bar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
        <span id="menu-text">Menu</span>
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 line">
      <div></div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-2 dash">
      <div class="right-full">
          <ul>
          <li>
            <a href="index.php">
              <img class="pic-active" src="image/bashboard-active.png">
              <img class="pic" src="image/bashboard.png">
              <p class="title-dash">DASHBOARD</p>
            </a>
          </li>
          <li>
            <a href="index2.php">
              <img class="pic-active" src="image/task-active.png">
              <img class="pic" src="image/task.png">
              <p class="title-dash">TASKS</p>
            </a>
          </li>
          <li>
            <a href="#">
              <img class="pic-active" src="image/messages-active.png">
              <img class="pic" src="image/messages.png">
              <p class="title-dash">MESSAGES</p>
            </a>
          </li>
          <li>
            <a href="#">
              <img class="pic-active" src="image/appointment-active.png">
              <img class="pic" src="image/appointment.png">
              <p class="title-dash">APPOINTMENTS</p>
            </a>
          </li>
          <li  class="active">
            <a href="index3.php">
              <img class="pic-active" src="image/travel-active.png">
              <img class="pic" src="image/travel.png">
              <p class="title-dash">TRAVEL</p>
            </a>
          </li>
          <li>
            <a href="#">
              <img class="pic-active" src="image/documents-active.png">
              <img class="pic" src="image/documents.png">
              <p class="title-dash">DOCUMENTS</p>
            </a>
          </li>
          <li>
            <a href="#">
              <img class="pic-active" src="image/call-us-active.png">
              <img class="pic" src="image/call-us.png">
              <p class="title-dash">CALL US</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="col-sm-10 prog col-sm-offsert-1">
      <div class="container-fluid main">
        <div class="row">
          <div class="col-xs-12">
            <h2>Travel Arrangements</h2>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <h3>Santa Barbara - Scripps Green Hospital</h3>
          </div>
          <div class="col-md-9">
            <p>Thu, May 26, 2016 - Wed Jun 1, 2016</p>
          </div>
          <div class="col-md-3 septask">
            <a href="#"><span class="glyphicon glyphicon-pencil"></span>Request itinerary changes</a>
          </div>
          <div class="col-xs-12">
            <div class="disc">
              <p>Thu, May 26</p>
            </div>
          </div>
        </div>
        <div class="row border-line">
          <div class="col-xs-1 icon-car">
            <img src="image/car-page3.png" style="width: 59%;">
          </div>
          <div class="col-xs-2 border-lin"><p>4:00 pm PST</p></div>
          <div class="col-xs-5 border-lin"><p>Hertz Rental Car pick-up</p></div>
          <div class="col-xs-4 septask"><a>View Details</a></div>
        </div>
        <div class="row border-line">
          <div class="col-xs-1 icon-bed">
            <img src="image/bed-page3.png" style="width: 59%;">
          </div>
          <div class="col-xs-2 border-lin"><p>8:00 pm PST</p></div>
          <div class="col-xs-5 border-lin"><p>Check-in Hilton La Jolla Torrey Pines</p></div>
          <div class="col-xs-4 septask"><a>View Details</a></div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div class="disc active">
              <p>Thu, May 26</p>
            </div>
          </div>
        </div>
        <div class="row border-line">
          <div class="col-xs-1 icon-car">
            <img src="image/car-page3.png" style="width: 59%;">
          </div>
          <div class="col-xs-2 border-lin"><p>11:00 am PST</p></div>
          <div class="col-xs-5 border-lin"><p>Check-in Hilton La Jolla Torrey Pines</p></div>
          <div class="col-xs-4"><p>Pending travel clearance</p></div>
        </div>
        <div class="row border-line">
          <div class="col-xs-1 icon-bed">
            <img src="image/bed-page3.png" style="width: 59%;">
          </div>
          <div class="col-xs-2 border-lin"><p>9:00 pm PST</p></div>
          <div class="col-xs-5 border-lin"><p>Hertz Rental Car drop-off</p></div>
          <div class="col-xs-4"><p>View Details</p></div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
