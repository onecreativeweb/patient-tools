<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<link href="icheck-1.x/skins/minimal/green.css" rel="stylesheet">
<script src="icheck-1.x/icheck.js"></script>
<script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
<script src="jquery.circliful.min.js"></script>
<body>
  <script>
  $(document).ready(function(){
    $('input').iCheck({
      checkboxClass: 'icheckbox_minimal-green',
      radioClass: 'iradio_minimal-green',
      increaseArea: '20%' // optional
    });
  });
  </script>
<div class="container-fluid" id="header">
  <div class="row">
    <div class="col-sm-3 text-center">
      <img id="pic-logo" src="image/logo.jpg">
    </div>
    <div class="col-sm-5 text-center col-sm-offset-1">
        <div class="row">
          <div class="col-sm-9 title" >
            <p>Welcome Wayne Enterpises Members</p>
          </div>
          <div class="col-sm-3">
            <img id="pic-cnlogo" src="image/cnlogo.jpg">
          </div>
        </div>
    </div>
    <div class="col-sm-2 text-center col-sm-offset-1">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <div id="menu-icon-bar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
        <span id="menu-text">Menu</span>
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 line">
      <div></div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-2 dash">
      <div class="right-full">
          <ul>
          <li>
            <a href="index.php">
              <img class="pic-active" src="image/bashboard-active.png">
              <img class="pic" src="image/bashboard.png">
              <p class="title-dash">DASHBOARD</p>
            </a>
          </li>
          <li  class="active">
            <a href="index2.php">
              <img class="pic-active" src="image/task-active.png">
              <img class="pic" src="image/task.png">
              <p class="title-dash">TASKS</p>
            </a>
          </li>
          <li>
            <a href="#">
              <img class="pic-active" src="image/messages-active.png">
              <img class="pic" src="image/messages.png">
              <p class="title-dash">MESSAGES</p>
            </a>
          </li>
          <li>
            <a href="#">
              <img class="pic-active" src="image/appointment-active.png">
              <img class="pic" src="image/appointment.png">
              <p class="title-dash">APPOINTMENTS</p>
            </a>
          </li>
          <li>
            <a href="index3.php">
              <img class="pic-active" src="image/travel-active.png">
              <img class="pic" src="image/travel.png">
              <p class="title-dash">TRAVEL</p>
            </a>
          </li>
          <li>
            <a href="#">
              <img class="pic-active" src="image/documents-active.png">
              <img class="pic" src="image/documents.png">
              <p class="title-dash">DOCUMENTS</p>
            </a>
          </li>
          <li>
            <a href="#">
              <img class="pic-active" src="image/call-us-active.png">
              <img class="pic" src="image/call-us.png">
              <p class="title-dash">CALL US</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="col-sm-10 prog col-sm-offsert-1">
      <div class="container-fluid main">
        <div class="row">
          <div class="col-xs-12">
              <h2>Tasks</h2>
          </div>
          <hr>
          <div class="col-xs-12">
            <h3>My Task</h3>
            <p>Pending: 0 (Carrum Care concierge is performing a task)</p>
            <div class="disc">
              <p class="disc-text1">Description</p>
              <p>Due</p>
            </div>

          </div>
        </div>
        <div class="row septask">
          <div class="col-xs-1 task-line"><input type="checkbox" checked></div>
          <div class="col-xs-7 task-line"><p>Add all your providers for medical record collection</p></div>
          <div class="col-xs-2 task-line3"><p>Done</p></div>
          <div class="col-xs-2 task-line"> <a href="#">Review</a></div>
        </div>
        <div class="row septask">
          <div class="col-xs-1 task-line"><input type="checkbox" checked></div>
          <div class="col-xs-7 task-line"><p>Sign medical record release form</p></div>
          <div class="col-xs-2 task-line3"><p>Done</p></div>
          <div class="col-xs-2 task-line"> <a href="#">Review</a></div>
        </div>
        <div class="row">
          <h3>Concierge Task</h3>
          <p>Waiting on 1 Care Concierge task</p>
          <div class="disc">
            <p class="disc-text1">Description</p>
            <p>Due</p>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-1 task-line"><input type="checkbox"></div>
          <div class="col-xs-7 task-line"><p>Collecting medical records  on your behalf</p></div>
          <div class="col-xs-4 task-line "><p>Feb 13,2016</p></div>

        </div>
        <div class="row">
          <div class="col-xs-1 col-xs-offset-1 task-line"><input type="checkbox" checked></div>
          <div class="col-xs-6 task-line"><p>Records from Dr. Rosen</p></div>
          <div class="col-xs-4 task-line"><p>Done</p></div>
        </div>
        <div class="row">
          <div class="col-xs-1 col-xs-offset-1 task-line"><input type="checkbox" checked></div>
          <div class="col-xs-6 task-line"><p>Records from Dr. Dinenberg</p></div>
          <div class="col-xs-4 task-line"><p>Done</p></div>
        </div>
        <div class="row">
          <div class="col-xs-1 col-xs-offset-1 .col-xs-2 task-line"><input type="checkbox"></div>
          <div class="col-xs-6 task-line"> <p>X-Rays and MRI images from Sansum Radiology</p></div>
          <div class="col-xs-4 task-line"><p>Feb 13,2016</p></div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
