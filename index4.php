<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="style.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<link href="icheck-1.x/skins/square/green.css" rel="stylesheet">
<script src="icheck-1.x/icheck.js"></script>
<script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
<script src="jquery.circliful.min.js"></script>

<body>




<div class="pet">
    <input  class="dve" type="checkbox"  checked="checked">

    <input class="dve"  type="checkbox">
    <input  class="dve" type="checkbox">

    <input class="dve"  type="checkbox" checked="checked">
    <input  class="dve" type="checkbox">

<div class="edno"></div>
</div>

<script>
$(document).ready(function(){
    var countChecked = function() {
      var n = $("#home input:checked").length;
      console.log(n);
    };
    countChecked();
    $( "input[type=checkbox]" ).on( "ifToggled", countChecked );

   $('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
   });

  $('.nav-tabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
  });
  $('#progress').circliful({
              foregroundColor: "#0A79CD",
              fillColor: 'transparent',
              backgroundColor: "#ADDCFF",
              animation:0,
              //animationStep: 10,
              foregroundBorderWidth: 17,
              backgroundBorderWidth: 17,
              percent: 75,
              fontColor: '#000',
              textAdditionalCss:"font-size:35px;font-weight: 700;",
              //targetTextSize:'25',
    });
});
</script>


<div class="container">
  <div class="row task clearfix">
<ul class="nav nav-tabs" role="tablist">
  <li role="presentation" class="text-center" >
    <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
        <div class="text-center">
          <div class="prog-line-full">
            <div class="circle-one"><span class="glyphicon glyphicon-ok blue"></span></div>
          </div>
            <div class="prog-task"><p>Preliminary Information</p></div>

        </div>
      </a>
    </li>
    <li role="presentation" class="text-center active">
      <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
        <div class="text-center">
        <div class="prog-line-full">
            <div class="circle-two">
              <div class="percent">
                <div id="progress"></div>
              </div>
          <!--   <div class="incomplete ">
              <div class="arc"></div>
              <span class="present">75%</span>
            </div> -->
          </div>
        </div>
        <div class="prog-task"><p>Program Qualification</p></div>

        </div>
      </a>
    </li>
    <li role="presentation" class="text-center">
      <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">
        <div class="text-center">
          <div class="prog-line-full">
            <div class="circle"></div>
          </div>
            <div class="prog-task"><p>Pre-procedure Preparation</p></div>

        </div>
      </a>
    </li>
    <li role="presentation" class="text-center">
      <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">
        <div class="text-center">
          <div class="prog-line-full">
            <div class="circle"></div>
          </div>
            <div class="prog-task"><p>Hospital Stay</p></div>
        </div>
      </a>
    </li>
    <li role="presentation" class="text-center">
      <a href="#settings1" aria-controls="settings1" role="tab" data-toggle="tab">
        <div class="text-center">
          <div class="prog-line-full">
            <div class="circle"></div>
          </div>
          <div class="prog-task"><p>Post-discharge Care</p></div>

        </div>
      </a>
    </li>
    <li role="presentation" class="text-center">
      <a href="#settings2" aria-controls="settings2" role="tab" data-toggle="tab">
        <div class="text-center">
          <div class="prog-line-full">
            <div class="circle1"></div>
          </div>
            <div class="prog-task"><p>Program Completion</p></div>

        </div>
      </a>
    </li>
 </ul>
 <div class="tab-content">
  <div role="tabpanel" class="tab-pane" id="home">
    <div class="container-fluid">
      <div class="row" style="    background: #f9f9f9;">
        <div class="col-xs-12">
          <div class="disc">
                <p>Step</p>
          </div>
        </div>
      </div>
      <div class="row septask">
        <div class="col-xs-1 task-line">
          <input type="checkbox" checked>
        </div>
        <div class="col-xs-9 task-line">
            <p>Eligibility verification</p>
        </div>
        <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
      </div>
      <div class="row septask">
        <div class="col-xs-1 task-line">
          <input type="checkbox" checked>
        </div>
        <div class="col-xs-9 task-line">
            <p>Basic medical history</p>
        </div>
        <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
      </div>
      <div class="row septask">
        <div class="col-xs-1 task-line">
          <input type="checkbox" checked>
        </div>
        <div class="col-xs-9 task-line">
            <p>Preliminary program fit</p>
        </div>
        <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
      </div>
      <div class="row septask">
        <div class="col-xs-1 task-line">
          <input type="checkbox" checked>
        </div>
        <div class="col-xs-9 task-line">
            <p>Medical records collection</p>
        </div>
        <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
      </div>
      <div class="row septask">
        <div class="col-xs-1 task-line">
          <input type="checkbox" checked>
        </div>
        <div class="col-xs-9 task-line">
            <p>Surgeon case review</p>
        </div>
        <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
      </div>
      <div class="row septask">
        <div class="col-xs-1 task-line">
          <input type="checkbox" checked>
        </div>
        <div class="col-xs-9 task-line">
            <p>Program acceptance decision</p>
        </div>
        <div class="col-xs-2 task-line">  <a href="#"> Review</a></div>
      </div>
    </div>
  </div>
  <div role="tabpanel" class="tab-pane active" id="profile">
    <div class="container-fluid">
      <div class="row" style="    background: #f9f9f9;">
        <div class="col-xs-12">
          <div class="disc">
                <p>Step</p>
          </div>
        </div>
      </div>
      <div class="row septask">
        <div class="col-xs-1 task-line">
          <input type="checkbox" checked>
        </div>
        <div class="col-xs-9 task-line">
            <p>Eligibility verification</p>
        </div>
        <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
      </div>
      <div class="row septask">
        <div class="col-xs-1 task-line">
          <input type="checkbox" checked>
        </div>
        <div class="col-xs-9 task-line">
            <p>Basic medical history</p>
        </div>
        <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
      </div>
      <div class="row septask">
        <div class="col-xs-1 task-line">
          <span class="in-progress"></span>
        </div>
        <div class="col-xs-9 task-line">
            <p>Preliminary program fit</p>
        </div>
        <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
      </div>
      <div class="row septask">
        <div class="col-xs-1 task-line">
          <input type="checkbox">
        </div>
        <div class="col-xs-9 task-line">
            <p>Medical records collection</p>
        </div>
        <div class="col-xs-2 task-line">  <a href="#"> &nbsp; </a></div>
      </div>
      <div class="row septask">
        <div class="col-xs-1 task-line">
          <input type="checkbox">
        </div>
        <div class="col-xs-9 task-line">
            <p>Surgeon case review</p>
        </div>
        <div class="col-xs-2 task-line">  <a href="#">&nbsp;</a></div>
      </div>
      <div class="row septask">
        <div class="col-xs-1 task-line">
          <input type="checkbox">
        </div>
        <div class="col-xs-9 task-line">
            <p>Program acceptance decision</p>
        </div>
        <div class="col-xs-2 task-line">  <a href="#"> &nbsp; </a></div>
      </div>
    </div>
  </div>
  <div role="tabpanel" class="tab-pane" id="messages">...</div>
  <div role="tabpanel" class="tab-pane" id="settings">...</div>
  <div role="tabpanel" class="tab-pane" id="settings1">...</div>
  <div role="tabpanel" class="tab-pane" id="settings2">...</div>
</div>
  </div>
</div>
</body>
</html>
