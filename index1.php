<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<link href="icheck-1.x/skins/square/green.css" rel="stylesheet">
<script src="icheck-1.x/icheck.js"></script>
<script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
<script src="jquery.circliful.min.js"></script>
<body>
  <script>
function percentNow(){
var n = $("#profile input:checked").length;
var c = 100/6;
percent = Math.round(c*n);
console.log(percent);

}
function roll (){
  $('#progress').empty();
  $('#progress').circliful({
              foregroundColor: "#0A79CD",
              fillColor: 'transparent',
              backgroundColor: "#ADDCFF",
              animation:0,
              //animationStep: 10,
              foregroundBorderWidth: 17,
              backgroundBorderWidth: 17,
              percent: percent,
              fontColor: '#000',
              textAdditionalCss:"font-size:35px;font-weight: 700;",
              //targetTextSize:'25',
    });
}


$(document).ready(function(){
        var percent = 0;
        var countChecked = function() {
        var n = $("#profile input:checked").length;
        var c = 100/6;
        percent = Math.round(c*n);
        console.log(percent);
        percentNow();
        roll();
      };
      countChecked();
      $( "input[type=checkbox]" ).on( "ifToggled", countChecked );

      var countChecked1 = function() {
        var n = $("#home input:checked").length;

        console.log(n);
      };
      countChecked1();
      $( "input[type=checkbox]" ).on( "ifToggled", countChecked1 );

     $('input').iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green',
      increaseArea: '20%' // optional
     });

    $('.nav-tabs a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
    });
  roll();
  });
</script>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="container-fluid" id="header">
        <div class="row">
          <div class="col-sm-3 no-padding">
            <img id="pic-logo" src="image/logo.jpg">
          </div>
          <div class="col-sm-7 text-center">
              <div class="row">
                <div class="col-sm-9 title" >
                  <p>Welcome Wayne Enterpises Members</p>
                </div>
                <div class="col-sm-3">
                  <img id="pic-cnlogo" src="image/cnlogo.jpg">
                </div>
              </div>
          </div>
          <div class="col-sm-2 text-center">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <div id="menu-icon-bar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </div>
          <!-- <span id="menu-text">Menu</span>-->
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="line"></div>
<div class="container-fluid" id="body-full">
<div class="row">
<div class="col-md-10 col-md-offset-1">
  <div class="container-fluid" id="body">
    <div class="row">
      <div class="col-sm-2 dash">
        <div class="right-full">
            <ul>
            <li class="active">
              <a href="index.php">
                <img class="pic-active" src="image/bashboard-active.png">
                <img class="pic" src="image/bashboard.png">
                <p class="title-dash">DASHBOARD</p>
              </a>
            </li>
            <li>
              <a href="index2.php">
                <img class="pic-active" src="image/task-active.png">
                <img class="pic" src="image/task.png">
                <p class="title-dash">TASKS</p>
              </a>
            </li>
            <li>
              <a href="#">
                <img class="pic-active" src="image/messages-active.png">
                <img class="pic" src="image/messages.png">
                <p class="title-dash">MESSAGES</p>
              </a>
            </li>
            <li>
              <a href="#">
                <img class="pic-active" src="image/appointment-active.png">
                <img class="pic" src="image/appointment.png">
                <p class="title-dash">APPOINTMENTS</p>
              </a>
            </li>
            <li>
              <a href="index3.php">
                <img class="pic-active" src="image/travel-active.png">
                <img class="pic" src="image/travel.png">
                <p class="title-dash">TRAVEL</p>
              </a>
            </li>
            <li>
              <a href="#">
                <img class="pic-active" src="image/documents-active.png">
                <img class="pic" src="image/documents.png">
                <p class="title-dash">DOCUMENTS</p>
              </a>
            </li>
            <li>
              <a href="#">
                <img class="pic-active" src="image/call-us-active.png">
                <img class="pic" src="image/call-us.png">
                <p class="title-dash">CALL US</p>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-sm-10 prog col-sm-offsert-1">
        <div class="container-fluid prog-line">
          <div class="row">
            <h2>Progress</h2>
          </div>
          <div class="row task clearfix">
            <div class="row task clearfix">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="text-center" >
              <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                  <div class="text-center">
                    <div class="prog-line-full">
                      <div class="circle-one"><span class="glyphicon glyphicon-ok blue"></span></div>
                    </div>
                      <div class="prog-task"><p>Preliminary Information</p></div>

                  </div>
                </a>
              </li>
              <li role="presentation" class="text-center active">
                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
                  <div class="text-center">
                  <div class="prog-line-full">
                      <div class="circle-two">
                        <div class="percent">
                          <div id="progress"></div>
                        </div>
                    <!--   <div class="incomplete ">
                        <div class="arc"></div>
                        <span class="present">75%</span>
                      </div> -->
                    </div>
                  </div>
                  <div class="prog-task"><p>Program Qualification</p></div>

                  </div>
                </a>
              </li>
              <li role="presentation" class="text-center">
                <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">
                  <div class="text-center">
                    <div class="prog-line-full">
                      <div class="circle"></div>
                    </div>
                      <div class="prog-task"><p>Pre-procedure Preparation</p></div>

                  </div>
                </a>
              </li>
              <li role="presentation" class="text-center">
                <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">
                  <div class="text-center">
                    <div class="prog-line-full">
                      <div class="circle"></div>
                    </div>
                      <div class="prog-task"><p>Hospital Stay</p></div>
                  </div>
                </a>
              </li>
              <li role="presentation" class="text-center">
                <a href="#settings1" aria-controls="settings1" role="tab" data-toggle="tab">
                  <div class="text-center">
                    <div class="prog-line-full">
                      <div class="circle"></div>
                    </div>
                    <div class="prog-task"><p>Post-discharge Care</p></div>

                  </div>
                </a>
              </li>
              <li role="presentation" class="text-center">
                <a href="#settings2" aria-controls="settings2" role="tab" data-toggle="tab">
                  <div class="text-center">
                    <div class="prog-line-full">
                      <div class="circle1"></div>
                    </div>
                      <div class="prog-task"><p>Program Completion</p></div>

                  </div>
                </a>
              </li>
           </ul>
           <div class="tab-content">
            <div role="tabpanel" class="tab-pane" id="home">
              <div class="container-fluid">
                <div class="row" style="    background: #f9f9f9;">
                  <div class="col-xs-12">
                    <div class="disc">
                          <p>Step</p>
                    </div>
                  </div>
                </div>
                <div class="row septask">
                  <div class="col-xs-1 task-line">
                    <input type="checkbox" checked>
                  </div>
                  <div class="col-xs-9 task-line">
                      <p>Eligibility verification</p>
                  </div>
                  <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
                </div>
                <div class="row septask">
                  <div class="col-xs-1 task-line">
                    <input type="checkbox" checked>
                  </div>
                  <div class="col-xs-9 task-line">
                      <p>Basic medical history</p>
                  </div>
                  <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
                </div>
                <div class="row septask">
                  <div class="col-xs-1 task-line">
                    <input type="checkbox" checked>
                  </div>
                  <div class="col-xs-9 task-line">
                      <p>Preliminary program fit</p>
                  </div>
                  <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
                </div>
                <div class="row septask">
                  <div class="col-xs-1 task-line">
                    <input type="checkbox" checked>
                  </div>
                  <div class="col-xs-9 task-line">
                      <p>Medical records collection</p>
                  </div>
                  <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
                </div>
                <div class="row septask">
                  <div class="col-xs-1 task-line">
                    <input type="checkbox" checked>
                  </div>
                  <div class="col-xs-9 task-line">
                      <p>Surgeon case review</p>
                  </div>
                  <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
                </div>
                <div class="row septask">
                  <div class="col-xs-1 task-line">
                    <input type="checkbox" checked>
                  </div>
                  <div class="col-xs-9 task-line">
                      <p>Program acceptance decision</p>
                  </div>
                  <div class="col-xs-2 task-line">  <a href="#"> Review</a></div>
                </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane active" id="profile">
              <div class="container-fluid">
                <div class="row" style="    background: #f9f9f9;">
                  <div class="col-xs-12">
                    <div class="disc">
                          <p>Step</p>
                    </div>
                  </div>
                </div>
                <div class="row septask">
                  <div class="col-xs-1 task-line">
                    <input type="checkbox" checked>
                  </div>
                  <div class="col-xs-9 task-line">
                      <p>Eligibility verification</p>
                  </div>
                  <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
                </div>
                <div class="row septask">
                  <div class="col-xs-1 task-line">
                    <input type="checkbox" checked>
                  </div>
                  <div class="col-xs-9 task-line">
                      <p>Basic medical history</p>
                  </div>
                  <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
                </div>
                <div class="row septask">
                  <div class="col-xs-1 task-line">
                    <span class="in-progress"></span>
                  </div>
                  <div class="col-xs-9 task-line">
                      <p>Preliminary program fit</p>
                  </div>
                  <div class="col-xs-2 task-line">  <a href="#">Review</a></div>
                </div>
                <div class="row septask">
                  <div class="col-xs-1 task-line">
                    <input type="checkbox">
                  </div>
                  <div class="col-xs-9 task-line">
                      <p>Medical records collection</p>
                  </div>
                  <div class="col-xs-2 task-line">  <a href="#"> &nbsp; </a></div>
                </div>
                <div class="row septask">
                  <div class="col-xs-1 task-line">
                    <input type="checkbox">
                  </div>
                  <div class="col-xs-9 task-line">
                      <p>Surgeon case review</p>
                  </div>
                  <div class="col-xs-2 task-line">  <a href="#">&nbsp;</a></div>
                </div>
                <div class="row septask">
                  <div class="col-xs-1 task-line">
                    <input type="checkbox">
                  </div>
                  <div class="col-xs-9 task-line">
                      <p>Program acceptance decision</p>
                  </div>
                  <div class="col-xs-2 task-line">  <a href="#"> &nbsp; </a></div>
                </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="messages">...</div>
            <div role="tabpanel" class="tab-pane" id="settings">...</div>
            <div role="tabpanel" class="tab-pane" id="settings1">...</div>
            <div role="tabpanel" class="tab-pane" id="settings2">...</div>
          </div>
            </div>
    </div>
  </div>
  </div>
</div>
</div>
</div>

</body>
</html>
